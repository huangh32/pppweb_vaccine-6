import threading, datetime
from PhmWeb.utils.dbconfig import get_PHM_db


class DbPhmRefReformatSource:
    _instance_lock = threading.Lock()

    def __init__(self):
        self._organ_measure_dict = dict()
        zw = get_PHM_db()
        self._all_data = list(zw['phm_ref_ReformatSource'].find())
        self._data_clinic_name_list = [x for x in self._all_data if x['DataType'] == 'ClinicName']


    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmRefReformatSource, "_instance"):
            with DbPhmRefReformatSource._instance_lock:  # 为了保证线程安全在内部加锁
                if not hasattr(DbPhmRefReformatSource, "_instance"):
                    DbPhmRefReformatSource._instance = DbPhmRefReformatSource(*args, **kwargs)
        return DbPhmRefReformatSource._instance

    def get_clinic_name_by_clinic_id(self, clinic_id):
        cli_list = [x for x in self._data_clinic_name_list if x['ClinicID'] == clinic_id]
        if len(cli_list) == 1:
            return cli_list[0].get('ClinicName')
        return None
