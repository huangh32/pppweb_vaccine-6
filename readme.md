###COVID Test Logic

1. If any answers are YES, eligible for COVID
2. If all answers are NO, and
    - 2a) symptoms started >10 days ago, or
    - 2b) no current symptoms >=3 days, or
    - 2c) any prior positive COVID tests, regardless of symptom time
    - THEN eligible for IMMUNITY
3. If all answers are NO and last day of symptoms are less than 10 days, not eligible for any test.

###Environment Variables
   - export PHMWEB_VACCINE_REDIS_HOST="192.168.168.106"
   - export ECLINIC_DB67_IP="192.168.168.37"
   - export ECLINIC_DB67_NAME="ec67"
   - export MDLAND_DC_MONGODB_IP="mongodb://192.168.168.176:27017,192.168.168.173:27017/"
   - export MDLAND_DC_MONGODB_ZW="zw"
   - export MDLAND_DC_MONGODB_DW="DW_Prod"
   - export ENVIRONMENT_TEST="DW_Prod"

###Environment Variables for test
   - export PHMWEB_VACCINE_REDIS_HOST="127.0.0.1"
   - export ECLINIC_DB67_IP="192.168.168.81"
   - export ECLINIC_DB67_NAME="ec67_2021"
   - export MDLAND_DC_MONGODB_IP="mongodb://192.168.168.176:27017,192.168.168.173:27017/"
   - export MDLAND_DC_MONGODB_ZW="zw_demo"
   - export MDLAND_DC_MONGODB_DW="DW_Prod"


### System Service
   - systemctl restart redis

### Release
   - /home/hu/uppppvac_all.sh
   - /home/hu/upvac.sh