from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import render


def index(request):
    page_dict = dict()
    return render(request, 'root/default.html', page_dict)

