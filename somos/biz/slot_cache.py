import threading
from PhmWeb.utils.dbconfig import sql_ec_db


class SlotCache:
    _instance_lock = threading.Lock()

    def __init__(self):
        print('SlotCache_init')
        self._location_dict = dict() # olid, period(5 or 10)

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(SlotCache, "_instance"):
            with SlotCache._instance_lock:  # for thread safe
                if not hasattr(SlotCache, "_instance"):
                    SlotCache._instance = SlotCache(*args, **kwargs)
                    SlotCache._instance.update_location_cache()
        return SlotCache._instance

    # get loction schedule period , 5, or 10
    def get_schedule_period_by_location(self, location_id):
        # cache solution
        if location_id not in self._location_dict:
            return 10
        ol = self._location_dict[location_id]
        return ol['ScheduleInterval']

    def get_apt_upper_limit_by_location(self, location_id):
        if location_id not in self._location_dict:
            return 1
        ol = self._location_dict[location_id]
        return ol['OLMaxApptCount'] if ol['OLMaxApptCount'] > 0 else 1

    #
    def update_location_cache(self):
        sql_ol = 'select OLID, OLScheduleInterval as ScheduleInterval, OLMaxApptCount  from Ref_ClinicOfficeLocation where OLScheduleInterval>1'
        ec_db = sql_ec_db(as_dict=True)
        ol_list = ec_db.excute_select(sql_ol)
        ec_db.close_conn()
        for ol in ol_list:
            olid = ol['OLID']
            self._location_dict[olid] = ol


    # 05/30/2020
    def update_location_interval_to_cache(self, ol_list):
        if ol_list is None:
            return
        for ol in ol_list:
            olid = ol['OLID']
            if olid in self._location_dict:
                self._location_dict[olid]['ScheduleInterval'] = ol['OLScheduleInterval']
                self._location_dict[olid]['OLMaxApptCount'] = ol['OLMaxApptCount']
            else:
                self._location_dict[olid] = dict(
                    {'OLID': olid,
                     'ScheduleInterval': ol['OLScheduleInterval'],
                     'OLMaxApptCount': ol['OLMaxApptCount']
                     }
                )
